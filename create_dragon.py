import GUI


class DragonCreation(GUI.GuiElements):
    dragon_looks = {'RACE': '', 'GENDER': '', 'BASE_COLOR': '', 'WING_COLOR': '', 'OVERLAY_COLOR': '',
                    'EYE_COLOR': '', 'PATTERN': ''}

    def on_mouse_press(self, x, y, button, modifiers):
        # +++++++++++++++++++++++++++++++++++++Dragon Editor Controls++++++++++++++++++++++++++++++++++++++++++++++++++
        """Logic behind determining loading of images based on user input is simple, but in editors like this there is
        often a lot more possible inputs and outputs. There's only 2 possible races in this program but script even
        at this moment is long.
        Pyglet has rather limited resource and GUI handling in situations like this so excessive use of if/else
        statements is unavoidable.
        It wouldn't be a good framework to use in full fledged editor, but for demo it's acceptable."""

        # if self.siren_label.x < x < (self.siren_label.x + self.siren_label.x) and \
        #         self.siren_label.y < y < (self.siren_label.y + self.siren_label.y):

        # Set dragon race and gender
        if 845 <= y <= 865 and 15 <= x <= 75:
            self.dragon_looks['RACE'] = self.current_race_label.text = 'siren'
            self.dragon_looks['GENDER'] = self.current_gender_label.text = 'male'
            self.basic_image = 'siren_male_dummy.png'
        if 825 <= y <= 845 and 15 <= x <= 75:
            self.dragon_looks['RACE'] = self.current_race_label.text = 'siren'
            self.dragon_looks['GENDER'] = self.current_gender_label.text = 'female'
            self.basic_image = 'siren_female_dummy.png'

        if 755 <= y <= 775 and 15 <= x <= 75:
            self.dragon_looks['RACE'] = self.current_race_label.text = 'imperial'
            self.dragon_looks['GENDER'] = self.current_gender_label.text = 'male'
            self.basic_image = 'viper_male_dummy.png'
        if 775 <= y <= 795 and 15 <= x <= 75:
            self.dragon_looks['RACE'] = self.current_race_label.text = 'imperial'
            self.dragon_looks['GENDER'] = self.current_gender_label.text = 'female'
            self.basic_image = 'viper_female_dummy.png'

        # Set base color
        if 710 <= y <= 725 and 15 <= x <= 75:
            self.dragon_looks['BASE_COLOR'] = self.current_base_label.text = 'albino'
        elif 685 <= y <= 705 and 15 <= x <= 75:
            self.dragon_looks['BASE_COLOR'] = self.current_base_label.text = 'celestial'
        elif 670 <= y <= 685 and 15 <= x <= 75:
            self.dragon_looks['BASE_COLOR'] = self.current_base_label.text = 'maleficent'
        elif 645 <= y <= 670 and 15 <= x <= 75:
            self.dragon_looks['BASE_COLOR'] = self.current_base_label.text = 'mint'
        elif 625 <= y <= 645 and 15 <= x <= 75:
            self.dragon_looks['BASE_COLOR'] = self.current_base_label.text = 'pastel'

        if self.dragon_looks['BASE_COLOR'] is not '':
            self.basic_image = self.dragon_looks.get('RACE') + '_' + self.dragon_looks.get('GENDER') + '_base_' + \
                               self.dragon_looks.get('BASE_COLOR') + '.png'
        else:
            pass

        # Set wing color
        if 575 <= y <= 590 and 15 <= x <= 75:
            self.dragon_looks['WING_COLOR'] = self.current_wing_label.text = 'bronze'
        elif 560 <= y <= 575 and 15 <= x <= 75:
            self.dragon_looks['WING_COLOR'] = self.current_wing_label.text = 'swamp'
        elif 545 <= y <= 560 and 15 <= x <= 75:
            self.dragon_looks['WING_COLOR'] = self.current_wing_label.text = 'gold'

        if self.dragon_looks['WING_COLOR'] is not '':
            self.wing_image = self.dragon_looks.get('RACE') + '_' + self.dragon_looks.get('GENDER') + '_wing_' + \
                              self.dragon_looks.get('WING_COLOR') + '.png'
        else:
            pass

        # Set patterns
        if 325 <= y <= 345 and 15 <= x <= 75:
            self.dragon_looks['PATTERN'] = self.current_pattern_label.text = 'pattern1'
        elif 305 <= y <= 325 and 15 <= x <= 75:
            self.dragon_looks['PATTERN'] = self.current_pattern_label.text = 'pattern2'
        elif 285 <= y <= 305 and 15 <= x <= 75:
            self.dragon_looks['PATTERN'] = self.current_pattern_label.text = 'pattern3'

        if self.dragon_looks['PATTERN'] is not '':
            self.pattern_image = self.dragon_looks.get('RACE') + '_' + self.dragon_looks.get('GENDER') + '_pattern_' + \
                              self.dragon_looks.get('PATTERN') + '.png'
        else:
            pass

        # Set overlay
        if 485 <= y <= 510 and 15 <= x <= 75:
            self.dragon_looks['OVERLAY_COLOR'] = self.current_overlay_label.text = 'violet'
        elif 465 <= y <= 485 and 15 <= x <= 75:
            self.dragon_looks['OVERLAY_COLOR'] = self.current_overlay_label.text = 'yellow'
        elif 445 <= y <= 465 and 15 <= x <= 75:
            self.dragon_looks['OVERLAY_COLOR'] = self.current_overlay_label.text = 'azure'

        if self.dragon_looks['OVERLAY_COLOR'] is not '':
            self.overlay_image = self.dragon_looks.get('RACE') + '_' + self.dragon_looks.get('GENDER') + '_overlay_' + \
                                 self.dragon_looks.get('OVERLAY_COLOR') + '.png'
        else:
            pass

        # Set eye color
        if 395 <= y <= 415 and 15 <= x <= 75:
            self.dragon_looks['EYE_COLOR'] = self.current_eye_label.text = 'red'
        elif 380 <= y <= 395 and 15 <= x <= 75:
            self.dragon_looks['EYE_COLOR'] = self.current_eye_label.text = 'yellow'

        if self.dragon_looks['EYE_COLOR'] is not '':
            self.eye_image = self.dragon_looks.get('RACE') + '_' + self.dragon_looks.get('GENDER') + '_eye_' + \
                             self.dragon_looks.get('EYE_COLOR') + '.png'
        else:
            pass

        if 10 <= y <= 40 and 330 <= x <= 440:
            self.parent1 = self.dragon_looks.copy()
            print(self.parent1)
            return self.parent1
        if 10 <= y <= 40 and 730 <= x <= 840:
            self.parent2 = self.dragon_looks.copy()
            print(self.parent2)
            return self.parent2

    # +++++++++++++++++++++++++++++++++++++/Dragon Editor Controls+++++++++++++++++++++++++++++++++++++++++++++++++
