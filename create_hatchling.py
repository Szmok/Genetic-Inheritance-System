import random
import create_dragon


class Inheritance(create_dragon.DragonCreation):
    def hatchling_race(self):
        if self.parent1.get('RACE') == self.parent2.get('RACE'):
            return self.parent1.get('RACE')
        elif self.parent1.get('RACE') != self.parent2.get('RACE'):
            self.hybrid_chance = random.randint(0, 100)
            if self.hybrid_chance <= 5:
                print('This hatchling is a hybrid')
                self.hybrid_mutation_chance = random.randint(0, 100)
                if self.hybrid_mutation_chance <= 40:
                    return 'This hybrid hatchling has a lethal mutation'
                elif self.hybrid_mutation_chance <= 65:
                    return 'This hybrid hatchling has a serious mutation'
                elif self.hybrid_mutation_chance <= 85:
                    return 'This hybrid hatchling has a minor mutation'
                else:
                    return 'This hybrid hatchling is healthy'
            else:
                return 'This egg died'

    #  GENDER
    def hatchling_gender(self):
        return random.choice(['female', 'male'])

    #  BASE COLOR
    base_typical = ['mint']
    base_odd = ['pastel']
    base_unnatural = ['maleficent']
    base_anomalous = ['celestial']
    base_bizarre = ['albino']

    def hatchling_base(self):
        self.base_type_chances = random.randint(0, 100)

        if self.base_type_chances <= 25:
            return self.parent1.get('BASE_COLOR')
        elif self.base_type_chances <= 50:
            return self.parent2.get('BASE_COLOR')
        elif self.base_type_chances <= 80:
            return random.choice(self.base_typical)
        elif self.base_type_chances <= 90:
            return random.choice(self.base_odd)
        elif self.base_type_chances <= 95:
            return random.choice(self.base_bizarre)
        elif self.base_type_chances <= 99:
            return random.choice(self.base_unnatural)
        elif self.base_type_chances == 100:
            return random.choice(self.base_anomalous)

    #  WING COLOR
    def hatchling_wing(self):
        return random.choice([self.parent1.get('WING_COLOR'), self.parent2.get('WING_COLOR')])

    #  BASE OVERLAY
    def hatchling_overlay(self):
        return random.choice([self.parent1.get('OVERLAY_COLOR'), self.parent2.get('OVERLAY_COLOR')])

    #  EYES
    def hatchling_eye(self):
        return random.choice([self.parent1.get('EYE_COLOR'), self.parent2.get('EYE_COLOR')])

    # PATTERNS
    def hatchling_pattern(self):
        return random.choice([self.parent1.get('PATTERN'), self.parent2.get('PATTERN')])

    # MUTATIONS
    def on_mouse_press(self, x, y, button, modifiers):
        if 550 <= x <= 740 and 10 <= y <= 50:
            print('Race: ', self.hatchling_race())
            print('Gender: ', self.hatchling_gender())
            print('Base color: ', self.hatchling_base())
            print('Wing color: ', self.hatchling_wing())
            print('Overlay: ', self.hatchling_overlay())
            print('Eyes: ', self.hatchling_eye())
            print('Patterns: ', self.hatchling_pattern())


class HatchlingRender(Inheritance):

    def hatchling_render(self):
        self.hatchling_base_image = self.hatchling_race + '_' + self.hatchling_gender + '_base_' \
                                    + self.hatchling_base + '.png'
        self.hatchling_wing_image = self.hatchling_race + '_wing_' + self.hatchling_wing + '.png'
        self.hatchling_overlay_image = self.hatchling_race + '_overlay_' + self.hatchling_overlay + '.png'
        self.hatchling_eye_image = self.hatchling_race + '_eye_' + self.hatchling_eye + '.png'
        self.hatchling_pattern_image = self.hatchling_race + '_pattern_' + self.hatchling_pattern + '.png'
