import pyglet
import create_dragon
import create_hatchling
import help_text
from time import time
from pyglet.window import key

# -------------------------------------- Info -------------------------------------------------------------------------
"""
Attributes:
WIDTH - width of window
HEIGHT - height of window
x - width offset
y - height offset
"""
# -------------------------------------- /Info ------------------------------------------------------------------------
# -------------------------------------- Settings ---------------------------------------------------------------------

WIDTH = 900
HEIGHT = 750

pyglet.resource.path = ['resources', 'resources/GUI', 'resources/Bases/siren/male',
                        'resources/Bases/siren/female', 'resources/Bases/viper/male', 'resources/Bases/viper/female']

pyglet.resource.reindex()

# -------------------------------------- /Settings --------------------------------------------------------------------

# Class that "listens" to mouse events in every screen and handles them, Main() inherits from this class to properly
# recognize mouse events and take appropriate action.
# MouseHandling inherits from other classes that contain mouse inputs and calls them.


class MouseHandling(create_dragon.DragonCreation):
    def on_mouse_press(self, x, y, button, modifiers):

        if button == pyglet.window.mouse.LEFT:  # Debugging, delete later
            print(x, y)

        # +++++++++++++++++++++++++++++++++++++GUI Controls++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        if 670 <= y <= 740 and 15 <= x <= 140 and type(self.currentScreen) is Menu:
            self.currentScreen = Editor()
        if 620 <= y <= 670 and 15 <= x <= 140 and type(self.currentScreen) is Menu:
            self.currentScreen = HatchlingScreen(x=500, y=HEIGHT // 2)
        if 580 <= y <= 620 and 15 <= x <= 140 and type(self.currentScreen) is Menu:
            self.currentScreen = Help(x=500, y=HEIGHT // 2)
        if 520 <= y <= 580 and 15 <= x <= 140 and type(self.currentScreen) is Menu:
            self.on_close()

        if self.RETURN_label.x < x < (self.RETURN_label.x + self.RETURN_label.x) and \
                self.RETURN_label.y < y < (self.RETURN_label.y + self.RETURN_label.y):
            self.currentScreen = Menu(x=500, y=HEIGHT // 2)

        # +++++++++++++++++++++++++++++++++++++/GUI Controls+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        # Call to outer classes containing all mouse events in creator of a dragon/hatchling
        if type(self.currentScreen) is Editor:
            create_dragon.DragonCreation.on_mouse_press(self, x, y, button, modifiers)
            create_hatchling.Inheritance.on_mouse_press(self, x, y, button, modifiers)

# -------------------------------------- Windows ----------------------------------------------------------------------


# Main function creating and formatting program window
class Background(pyglet.sprite.Sprite):
    def __init__(self, texture=None, width=WIDTH, height=HEIGHT, color=(83, 92, 104, 255), x=0, y=0):
        if texture is None:
            self.texture = pyglet.image.SolidColorImagePattern(color).create_image(width, height)
        else:
            self.texture = texture
        super(Background, self).__init__(self.texture)

        self.image.anchor_x = self.image.width / 2
        self.image.anchor_y = self.image.height / 2

        self.x = x
        self.y = y

    def _draw(self):
        self.draw()


class IntroScreen(Background):
    def __init__(self, texture=None, width=WIDTH, height=HEIGHT, x=450, y=0, color=(255, 204, 230, 255)):
        super(IntroScreen, self).__init__(texture, width=width, height=height, x=x, y=y, color=color)

        self.logo = pyglet.resource.image('logo.png')
        self.is_visible = time()

        self.intro_text = pyglet.text.Label('An experimental artificial pet editor', font_size=8,
                                            font_name='Verdana',
                                            x=x, y=y, multiline=False, width=width, height=height,
                                            color=(0, 0, 0, 255), anchor_x='center')

    def _draw(self):
        self.draw()
        self.intro_text.draw()
        if time() - 3 > self.is_visible:
            self.intro_text.text = 'with basic genetic inheritance system'


class Menu(Background):
    def __init__(self, width=WIDTH, height=HEIGHT, x=0, y=0):
        super(Menu, self).__init__(width=width, height=height, x=x, y=y)

        self.load_gustav = pyglet.resource.image('Gustav.png')
        self.Gustav = pyglet.sprite.Sprite(img=self.load_gustav)

    def _draw(self):
        self.draw()
        self.Gustav.draw()


class Editor(Background):
    def __init__(self, width=WIDTH, height=HEIGHT, x=0, y=0):
        super(Editor, self).__init__(width=width, height=height, x=x, y=y)

        self.editor_bg = pyglet.resource.image('editor_bg.png')

    def _draw(self):
        self.editor_bg.blit(0, 0)


class HatchlingScreen(Background):
    def __init__(self, width=WIDTH, height=HEIGHT, x=0, y=0):
        super(HatchlingScreen, self).__init__(width=width, height=height, x=x, y=y)

        self.hatchling_editor_bg = pyglet.resource.image('hatchling_bg.png')

    def _draw(self):
        self.hatchling_editor_bg.blit(0, 0)


class Help(Background):
    def __init__(self, width=WIDTH, height=HEIGHT, x=0, y=0):
        super(Help, self).__init__(width=width, height=height, x=x, y=y)

        self.siren_both = pyglet.resource.image('siren_both.png')
        self.help_dragons = pyglet.sprite.Sprite(img=self.siren_both, x=200, y=0)

    def _draw(self):
        self.draw()
        self.help_dragons.draw()


class Main(pyglet.window.Window, MouseHandling, create_hatchling.Inheritance, help_text.HelpText):
    def __init__(self):
        super(Main, self).__init__(900, 750, resizable=False, caption='[DEMO] Winged - Inheritance System')
        self.is_running = True

        self.currentScreen = Menu(x=450, y=HEIGHT // 2, width=WIDTH)  # Sets current screen
        self.screen_time = time()

        self.basic_image = 'siren_male_base_albino.png'
        self.wing_image = self.eye_image = self.overlay_image = self.pattern_image = 'Blank.png'
        self.dragon_batch = pyglet.graphics.Batch()

        self.dragon_base = pyglet.graphics.OrderedGroup(0)
        self.dragon_details = pyglet.graphics.OrderedGroup(1)

        self.create_labels()
        self.teext()

    def on_draw(self):
        self.render()

    def on_key_press(self, symbol, modifiers):
        if symbol == key.ESCAPE:
            self.on_close()

    def render(self):  # Main rendering method
        if time() - 5 > self.screen_time and type(self.currentScreen) is IntroScreen:  # Changes screen to Menu
            self.currentScreen = Menu(x=500, y=HEIGHT // 2)

        self.currentScreen._draw()  # Draws current screen

        if type(self.currentScreen) is Menu:
            self.draw_menu_elements()

        if type(self.currentScreen) is Help:
            self.draw_help_elements()
            self.teext_draw()

        if type(self.currentScreen) is HatchlingScreen:
            # self.draw_hatchling_elements()
            pass

        if type(self.currentScreen) is Editor:
            #self.draw_editor_elements()

            self.base_color_image = pyglet.resource.image(self.basic_image)
            self.base_sprite = pyglet.sprite.Sprite(self.base_color_image, x=280, y=200, batch=self.dragon_batch,
                                                    group=self.dragon_base)

            self.wing_color_image = pyglet.resource.image(self.wing_image)
            self.wing_sprite = pyglet.sprite.Sprite(self.wing_color_image, x=280, y=200, batch=self.dragon_batch,
                                                    group=self.dragon_base)

            self.eye_color_image = pyglet.resource.image(self.eye_image)
            self.eye_sprite = pyglet.sprite.Sprite(self.eye_color_image, x=280, y=200, batch=self.dragon_batch,
                                                   group=self.dragon_details)

            self.pattern_res = pyglet.resource.image(self.pattern_image)
            self.pattern_sprite = pyglet.sprite.Sprite(self.pattern_res, x=280, y=200, batch=self.dragon_batch,
                                                       group=self.dragon_details)

            self.overlay_res = pyglet.resource.image(self.overlay_image)
            self.overlay_sprite = pyglet.sprite.Sprite(self.overlay_res, x=280, y=200, batch=self.dragon_batch,
                                                       group=self.dragon_details)

            self.dragon_batch.draw()

        self.flip()

    def on_close(self):  # Detects pressing ESC key or closing window and stops the program
        self.is_running = 0

    def run(self):  # Replaces default pyglet.app.run() event. Starts the program.
        while self.is_running is True:
            self.render()
            self.dispatch_events()


program = Main()
program.run()
