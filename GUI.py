import pyglet


# Onyx, IrisUPC, Niagara Solid, Gabriola
FONT_NAME = 'Gabriola'


class GuiElements:
    editor_elements_batch = pyglet.graphics.Batch()

    # ++++++++++++++++++++++++++ Editor labels ++++++++++++++++++++++++++++++++++++++++++++++++++
    def editor_big_label_template(self, label, y, x):
        self.big_text = pyglet.text.Label(label,
                                          font_name=FONT_NAME,
                                          font_size=18,
                                          x=x,
                                          y=y,
                                          anchor_x='left',
                                          anchor_y='baseline',
                                          batch=self.editor_elements_batch)
        return self.big_text

    def editor_small_label_template(self, label, y, x):
        self.small_text = pyglet.text.Label(label,
                                            font_name=FONT_NAME,
                                            font_size=16,
                                            x=x,
                                            y=y,
                                            anchor_x='left',
                                            anchor_y='baseline',
                                            batch=self.editor_elements_batch)
        return self.small_text

    def button_template(self, label, y, x):
        self.menu_text = pyglet.text.Label(label,
                                           font_name=FONT_NAME,
                                           font_size=34,
                                           x=x,
                                           y=y,
                                           anchor_x='left',
                                           anchor_y='baseline')
        return self.menu_text

    def small_button_template(self, label, y, x):
        self.small_button_text = pyglet.text.Label(label,
                                                   font_name=FONT_NAME,
                                                   font_size=24,
                                                   x=x,
                                                   y=y,
                                                   anchor_x='left',
                                                   anchor_y='baseline')
        return self.small_button_text

    def dragon_label_template(self, label, y, x):
        self.dragon_button_text = pyglet.text.Label(label,
                                                    font_name=FONT_NAME,
                                                    font_size=24,
                                                    x=x,
                                                    y=y,
                                                    anchor_x='left',
                                                    anchor_y='baseline',
                                                    batch=self.editor_elements_batch)
        return self.dragon_button_text

    # ++++++++++++++++++++++++++ /Editor labels +++++++++++++++++++++++++++++++++++++++++++++++++

    def create_big_label(self, label, y, x):
        self.label_template = self.editor_big_label_template(label=label, y=y, x=x)
        return self.label_template

    def create_small_label(self, label, y, x):
        self.label_template = self.editor_small_label_template(label=label, y=y, x=x)
        return self.label_template

    def create_button(self, label, y, x):
        self.label_template = self.button_template(label=label, y=y, x=x)
        return self.label_template

    def create_small_button(self, label, y, x):
        self.label_template = self.small_button_template(label=label, y=y, x=x)
        return self.label_template

    def create_dragon_label(self, label, y, x):
        self.label_template = self.dragon_label_template(label=label, y=y, x=x)
        return self.label_template

    def create_labels(self):

        self.BREED_label = self.create_big_label('BREED AND GENDER', 870, 20)
        self.sirenm_label = self.create_small_label('Siren male', 850, 20)
        self.sirenf_label = self.create_small_label('Siren female', 830, 20)

        self.viperm_label = self.create_small_label('Viper male', 800, 20)
        self.viperf_label = self.create_small_label('Viper female', 780, 20)

        self.BASE_label = self.create_big_label('BASE', 730, 20)
        self.albino_label = self.create_small_label('Albino', 710, 20)
        self.celestial_label = self.create_small_label('Celestial', 690, 20)
        self.maleficent_label = self.create_small_label('Maleficent', 670, 20)
        self.mint_label = self.create_small_label('Mint', 650, 20)
        self.pastel_label = self.create_small_label('Pastel', 630, 20)

        self.WING_label = self.create_big_label('WING', 600, 20)
        self.bronze_wing_label = self.create_small_label('Bronze', 580, 20)
        self.swamp_wing__label = self.create_small_label('Swamp', 560, 20)
        self.gold_wing_label = self.create_small_label('Gold', 540, 20)

        self.OVERLAY_label = self.create_big_label('OVERLAY', 510, 20)
        self.violet_overlay_label = self.create_small_label('Violet', 490, 20)
        self.yellow_overlay_label = self.create_small_label('Yellow', 470, 20)
        self.azure_overlay_label = self.create_small_label('Azure', 450, 20)

        self.EYE_label = self.create_big_label('EYE', 420, 20)
        self.red_eye__label = self.create_small_label('Red', 400, 20)
        self.yellow_eye_label = self.create_small_label('Yellow', 380, 20)

        self.PATTERN_label = self.create_big_label('PATTERN', 350, 20)
        self.pattern1_label = self.create_small_label('Pattern1', 330, 20)
        self.pattern2_label = self.create_small_label('Pattern2', 310, 20)
        self.pattern3_label = self.create_small_label('Pattern3', 290, 20)

        self.EDITOR_label = self.create_button('EDITOR', 680, 20)
        self.INHERITANCE_label = self.create_button('INHERITANCE', 630, 20)
        self.HELP_label = self.create_button('HELP', 580, 20)
        self.QUIT_label = self.create_button('QUIT', 530, 20)

        self.RETURN_label = self.create_button('<-- RETURN', 20, 20)
        self.GENERATE_label = self.create_button('GENERATE', 20, 570)
        self.PARENT1_label = self.create_small_button('Save as first parent', 20, 340)
        self.PARENT2_label = self.create_small_button('Save as second parent', 20, 760)

        self.D_race_label = self.create_dragon_label('Breed:', 300, 340)
        self.D_gender_label = self.create_dragon_label('Gender:', 270, 340)
        self.D_base_label = self.create_dragon_label('Base color:', 240, 340)
        self.D_wing_label = self.create_dragon_label('Wing color:', 210, 340)
        self.D_overlay_label = self.create_dragon_label('Overlay:', 180, 340)
        self.D_eye_label = self.create_dragon_label('Eye color:', 150, 340)
        self.D_pattern_label = self.create_dragon_label('Pattern:', 120, 340)

        self.current_race_label = self.create_dragon_label('', 300, 460)
        self.current_gender_label = self.create_dragon_label('', 270, 460)
        self.current_base_label = self.create_dragon_label('', 240, 460)
        self.current_wing_label = self.create_dragon_label('', 210, 460)
        self.current_overlay_label = self.create_dragon_label('', 180, 460)
        self.current_eye_label = self.create_dragon_label('', 150, 460)
        self.current_pattern_label = self.create_dragon_label('', 120, 460)

    def draw_editor_elements(self):
        self.GENERATE_label.draw()
        self.PARENT1_label.draw()
        self.PARENT2_label.draw()
        self.RETURN_label.draw()

        self.editor_elements_batch.draw()

    def draw_menu_elements(self):
        self.EDITOR_label.draw()
        self.HELP_label.draw()
        self.QUIT_label.draw()
        self.INHERITANCE_label.draw()

    def draw_help_elements(self):
        self.RETURN_label.draw()
