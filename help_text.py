import pyglet

FONT_NAME = 'IrisUPC'


class HelpText(object):
    help_elements_batch = pyglet.graphics.Batch()

    def plain_text(self, label, y, x):
        self.big_text = pyglet.text.Label(label,
                                          font_name=FONT_NAME,
                                          font_size=18,
                                          x=x,
                                          y=y,
                                          anchor_x='left',
                                          anchor_y='baseline',
                                          batch=self.help_elements_batch)
        return self.plain_text

    def teext(self):
        self.document = pyglet.text.decode_text('This program is an experimental editor')

        self.text_layout = pyglet.text.layout.TextLayout(self.document, 800, 600, multiline=True, wrap_lines=True)

    def teext_draw(self):
        self.text_layout.draw()
