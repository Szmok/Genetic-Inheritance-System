# Install Requirements

Run following commands:
```
pip install git+https://github.com/jorgecarleitao/pyglet-gui.git
pip install -r requirements.txt
```
